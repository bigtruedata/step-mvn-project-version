#!/bin/bash

VERSION=$(grep '<version>' pom.xml \
          | head --lines=1 \
          | cut --delimiter='>' --fields=2 \
          | cut --delimiter='<' --field=1)

export MVN_PROJECT_VERSION="${VERSION}"
