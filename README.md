# Step mvn-project-version

Exports the project version in the `pom.xml` as the
`${MVN_PROJECT_VERSION}` environment variable.  The resulting variable
is build using the value of the `version` field.

Given the following chunk of a `pom.xml` file:

```xml
<version>1.2.3-SNAPSHOT</version>
```

The resulting variable would have the value:

```bash
MVN_PROJECT_NAME=1.2.3-SNAPSHOT
```

Using environment variable allows to use its value in the next steps
defined in the `wercker.yml` file.

# Options

This steps does not have any option.

# Example

```yaml
build:
  after-steps:
    - bigtruedata/mvn-project-version
```

# License

The MIT License (MIT)

# Changelog

## 0.1.0

- Initial release

